'use strict';

var gulp = require('gulp');
var path = require('path');
var download = require('gulp-download');
var unzip = require('gulp-unzip');
var rename = require('gulp-rename');
var filter = require('gulp-filter');
var ejs = require('gulp-ejs');
var config = require('./config');


gulp.task('clean-seed', require('del').bind(null, ['.tmp', 'dev']));


gulp.task('download', function() {
    return download(config.themeUrl)
        .pipe(unzip())
        .pipe(gulp.dest(config.tmpDir));
});


gulp.task('copy-dir', [/*'download'*/], function() {
	var dir = path.join(config.tmpDir, config.theme);

	return gulp.src([path.join(dir, 'app/**'), path.join(dir, 'template/**')], {
            base: dir
        })
        .pipe(gulp.dest(config.devDir));
});


gulp.task('copy-dev-task', function() {
	var dir = path.join(config.tmpDir, config.theme);

	return gulp.src(path.join(dir, 'gulpfile.js'), {
            base: dir
        })
		.pipe(rename('development.js'))
        .pipe(gulp.dest('./gulp-tasks/app'));
});


gulp.task('tkt-seed', ['copy-dir', 'copy-dev-task'], function() {
	var dir = path.join(config.tmpDir, config.theme);
    var filterHTML  = filter('**/*.html', {restore: true});
    var filterJSON  = filter('**/*.json', {restore: true});
    var filterJS    = filter('**/*.js',   {restore: true});

    return gulp.src([path.join(dir,'/app/index.html'), path.join(dir,'/bower.json'), path.join(dir,'/package.json'), path.join(dir,'/app/scripts/**/*')], {
            base: dir
        })
        .pipe(filterHTML)
        .pipe(ejs(config, {ext: '.html'}))
        .pipe(filterHTML.restore)
        .pipe(filterJSON)
        .pipe(ejs(config, {ext: '.json'}))
        .pipe(filterJSON.restore)
        .pipe(filterJS)
        .pipe(ejs(config, {ext: '.js'}))
        .pipe(filterJS.restore)
        .pipe(gulp.dest(config.devDir));
});
