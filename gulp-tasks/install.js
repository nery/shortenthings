'use strict';

var gulp = require('gulp');
var download = require('gulp-download');
var unzip = require('gulp-unzip');
var ejs = require('gulp-ejs');
var _ = require('lodash');
var Store = require('easy-jsonfs');
var filter = require('gulp-filter');
var tktcfg = require('./config');


gulp.task('download', function() {
    return download(tktcfg.themeUrl)
        .pipe(unzip())
        .pipe(gulp.dest(tktcfg.tmpDir));
});


gulp.task('copy-app-dir', ['download'], function() {
    var dir = tktcfg.themeDir;

    gulp.src([dir + '.jscsrc', dir + '.jshintrc'], {
            base: tktcfg.themeDir
        })
        .pipe(gulp.dest(tktcfg.devDir));

    return gulp.src([tktcfg.themeDir + 'app/**/*', tktcfg.themeDir + 'templates/**/*'], {
            base: tktcfg.themeDir
        })
        .pipe(gulp.dest(tktcfg.devDir));
});



gulp.task('init', ['copy-app-dir'], function() {
    // var files = ['app/index.html', 'bower.json'].map(function(fileName) {
    //     return tktcfg.themeDir + fileName;
    // });

    var store = new Store('./tekton.json');
    var conf = _.assign(tktcfg, store.getAll());
    var dir = tktcfg.themeDir;

    store.set(conf);

    var filterHTML  = filter('**/*.html', {restore: true});
    var filterJSON  = filter('**/*.json', {restore: true});
    var filterJS    = filter('**/*.js',   {restore: true});

    console.log(tktcfg);


    return gulp.src([dir + 'app/index.html', dir + 'bower.json', dir + 'package.json', dir + 'gulpfile.js',
        dir + 'app/scripts/**/*'], {
            base: tktcfg.themeDir
        })
        .pipe(filterJSON)
        .pipe(ejs(tktcfg, {ext: '.json'}))
        .pipe(filterJSON.restore)
        .pipe(filterHTML)
        .pipe(ejs(tktcfg))
        .pipe(filterHTML.restore)
        .pipe(filterJS)
        .pipe(ejs(tktcfg, {ext: '.js'}))
        .pipe(filterJS.restore)
        .pipe(gulp.dest(tktcfg.devDir));
});
