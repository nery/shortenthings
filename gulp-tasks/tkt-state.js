'use strict';

var gulp = require('gulp');
var path = require('path');
var change = require('gulp-change');
var inquirer = require("inquirer");
var config = require('./config');


gulp.task('tkt-state', function() {

    console.log("-- Create new state ui-router -------------------------");

    function askView(state) {
        inquirer.prompt(questionsView, function(answers) {
            state.views = state.views || [];
            state.views.push(answers);

            if (answers.askAgain) {
                askView(state);
            } else {
                generateState(state);
            }
        });
    }


    function generateState(state) {

        function chgState(content) {
            return content.replace(/(.state(?![\s\S]*.state)[\s\S]*?}\);)/g, code + '\t\t\t$1');
        }

        var codeState = [".state('<%= nome %>', {\n",
          "\t\t\t\turl: '/<%= nome %>'",
          "<% if (parent) { %>,\n\t\t\t\tparent: '<%= parent %>'<% } %>",
          "<% if (templateUrl) { %>,\n\t\t\t\ttemplateUrl: '<%= templateUrl %>'<% } %>",
          "<% if (controller) { %>,\n\t\t\t\tcontroller: '<%= controller %>'<% } %>",
          "\n\t\t\t})\n"];

        var codeStateViews = `.state('<%= nome %>', {
                url: '/<%= nome -%>'
<% if (parent) { %>,\n\t\t\t\tparent: '<%= parent %>',<% } %>
\t\t\t\tviews: {
<% views.forEach(function(view, idx){ -%><% if (idx > 0) { %>,\n<% } -%>
\t\t\t\t\t'<%= view.state %>': {templateUrl: '<%= view.templateUrl %>'<% if (view.controller !== '') { -%>
, controller: '<%= view.controller %>'<% } -%>}<% }); %>
\t\t\t\t}
\t\t\t})\n`;

        var tplCode = (state.multipleViews) ? codeStateViews : codeState.join("");

        var ejs = require('ejs'); 
        var code = ejs.render(tplCode, state);

        console.log(code);

        return gulp.src(path.join(config.devDir,'app/scripts/app.js'), {
                base: config.devDir
            })
            .pipe(change(chgState))
            .pipe(gulp.dest(config.devDir));  
    }

    var questionsView = [{
        type: "input",
        name: "state",
        message: "Nome do 'state'?"
    }, {
        type: "input",
        name: "templateUrl",
        message: "Qual a url ('templateUrl')?"
    }, {
        type: "input",
        name: "controller",
        message: "Qual o nome do 'controller'?"
    }, {
        type: "confirm",
        name: "askAgain",
        message: "Registrar outra 'view' para o 'state'?",
        default: true
    }];

    var questions = [{
        type: "input",
        name: "nome",
        message: "Qual o nome do novo 'state'?"
    }, {
        type: "input",
        name: "parent",
        message: "Nome da 'parent view'?"
    }, {
        type: "confirm",
        name: "multipleViews",
        message: "O novo 'state' possui multiplas 'views'?",
        default: false
    }, {
        type: "input",
        name: "templateUrl",
        message: "Qual a url ('templateUrl')?",
        when: function(answers) {
            return !answers.multipleViews;
        }
    }, {
        type: "input",
        name: "controller",
        message: "Qual o nome do 'controller'?",
        when: function(answers) {
            return !answers.multipleViews;
        }
    }];

    // console.log('config: ', config);

    setTimeout(function() {
        inquirer.prompt(questions, function(answers) {

            if (answers.multipleViews) {
                askView(answers);
            } else {
                generateState(answers);
            }

        });
    }, 1000);

});
