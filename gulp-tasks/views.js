'use strict';

var gulp = require('gulp');
var argv = require('yargs').argv;
var path = require('path');
var _ = require('lodash');
var ejs = require('gulp-ejs');
var rename = require('gulp-rename');
var Store = require('easy-jsonfs');
// var beautify = require('../easy-beautify.js');
var beautify = require('easy-beautify');
var tktcfg = require('./config');

gulp.task('tekton-view', [], function() {
    // var files = ['/app/index.html', '/bower.json'].map(function(fileName) {
    //     return cfg.tmpDir + cfg.theme + fileName;
    // });	

	var viewName = argv.n || argv.name;
	var dataFile = argv.f || argv.datafile || "data";
	var data = argv.d || argv.data;
	var tplView = argv.t || argv.template || "view";

	if (viewName === undefined || dataFile === undefined) {
		console.log("\nUSAGE: gulp tekton-view --name <view_name> --datafile <file_name> [--data <json_attr>] [--template <tpl_view>]");
		console.log("   view_name - nome do arquivo onde a view será gerada");
		console.log("   file_name - nome do arquivo que será utilizado como fonte de dados para a geração da view");
		console.log("   json_attr - atributo/objeto dentro do json/fonte de dados a ser utilizado");
		console.log("   tpl_view - template/arquivo (sem extensão) que deverá ser utilizadona geração da view\n");

		return;
	}

	tplView = tplView || "view";
	dataFile = dataFile || "data";

	var conf = tktcfg; //store.getAll();
	var dataView = Store.readJSON(path.join(/*process.cwd(),*/ conf.dataDir, dataFile));
	var optsHtml = {
	    "indent_size": 2,
	    "indent_char": " ",
	    "eol": "\n",
	    "indent_level": 0,
	    "indent_with_tabs": false,
	    "preserve_newlines": false,
	    "end-with-newline": true,
	    "beautifier": "html"
	};

	console.log("Path: ", path.join(/*process.cwd(),*/ conf.dataDir, dataFile));
    console.log("Running tekton-view...\nTemplate: ", tplView, "\ndataView: ", dataView);

	
	dataView = _.assign(conf, ((data !== undefined) ? _.get(dataView, data) : dataView));
	console.log("dataView: ", dataView);
	// console.log("dataFile: ", dataFile);


    return gulp.src(conf.templateDir  + "app/views/" + tplView + ".ejs", {base: conf.templateDir})
        .pipe(ejs(dataView))
        .pipe(rename({basename: viewName}))
        .pipe(beautify(optsHtml))
        .pipe(gulp.dest(conf.devDir));

});
