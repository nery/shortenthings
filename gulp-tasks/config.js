'use strict';

var gulp = require('gulp');
var _ = require('lodash');
var _s = require('underscore.string');
var argv = require('yargs').argv;
var Store = require('easy-jsonfs');

var store = new Store('./tekton.json');

/*
FROM:  https://drive.google.com/file/d/FILE_ID/edit?usp=sharing
TO:    https://drive.google.com/uc?export=download&id=FILE_ID
https://drive.google.com/open?id=0B46xa8AVPnvAVm4zUU9uZEJnVDQ
*/

var cfg = {
	nomeAplicacao: process.cwd().split('\\').slice(-1).pop(),
	autor: 'Softbox',
	emailAutor: 'softboxlab@gmail.com',
	configFile: 'tekton.json',
    // devDir: './dev/',
    devDir: './dev/',
    distDir: './dist/',
    tmpDir: './.tmp/',
    theme: 'ani-theme',
    // themeUrl: 'http://goo.gl/41n0HD?gdriveurl'
    //themeUrl: 'https://drive.google.com/uc?export=download&id=0B46xa8AVPnvAVm4zUU9uZEJnVDQ'
    themeUrl: 'http://srv4937.uppcdn.com/dl/kvoagko3yexwt63mxuf455fp6mcorxafniytped7s32vh4lpchbtc7ni/ani-theme.zip'
};
cfg.themeDir = cfg.tmpDir + cfg.theme + '/';
cfg.templateDir = cfg.devDir + 'templates/';
cfg.dataDir = cfg.devDir + 'data/';
cfg.tituloIndice = _s(cfg.nomeAplicacao).capitalize().value();


gulp.task('config', function() {
	var conf = _.assign(cfg, store.getAll());;

	if (argv.set) {
		store.set.apply(store, argv.set.split("="));
	} 

	if (argv.remove) {
		store.remove(argv.remove);
	}
	console.log(store.json);
});


function getConfiguration() {
	// var store = new Store('./tekton.json');
	var conf = store.getAll();

	conf = _.assign(cfg, conf);
	conf.descricao = (conf.descricao) ? conf.descricao : conf.nomeAplicacao;
	store.set(conf);

	return conf;
}


module.exports = getConfiguration();