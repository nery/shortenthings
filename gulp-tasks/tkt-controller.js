'use strict';

var gulp = require('gulp');
var path = require('path');
var change = require('gulp-change');
var inquirer = require("inquirer");
var config = require('./config');


gulp.task('tkt-controller', function() {

    console.log("-- Create new angular controller -------------------------");

    function generateState(state) {

        function chgState(content) {
            return content.replace(/(.state(?![\s\S]*.state)[\s\S]*?}\);)/g, code + '\t\t\t$1');
        }

        var codeState = [".state('<%= nome %>', {\n",
          "\t\t\t\turl: '/<%= nome %>'",
          "<% if (parent) { %>,\n\t\t\t\tparent: '<%= parent %>'<% } %>",
          "<% if (templateUrl) { %>,\n\t\t\t\ttemplateUrl: '<%= templateUrl %>'<% } %>",
          "<% if (controller) { %>,\n\t\t\t\tcontroller: '<%= controller %>'<% } %>",
          "\n\t\t\t})\n"];

        var codeStateViews = `.state('<%= nome %>', {
                url: '/<%= nome -%>'
<% if (parent) { %>,\n\t\t\t\tparent: '<%= parent %>',<% } %>
\t\t\t\tviews: {
<% views.forEach(function(view, idx){ -%><% if (idx > 0) { %>,\n<% } -%>
\t\t\t\t\t'<%= view.state %>': {templateUrl: '<%= view.templateUrl %>'<% if (view.controller !== '') { -%>
, controller: '<%= view.controller %>'<% } -%>}<% }); %>
\t\t\t\t}
\t\t\t})\n

        var ControllerTemplate = `'use strict';

/**
 * @ngdoc function
 * @name <%= nomeAplicacao %>.controller:<%= controllerName %>Ctrl
 * @description
 * # MainCtrl
 * Controller of <%= appName %>
 */
angular.module('<%= nomeAplicacao %>')
  .controller('<%= controllerName %>Ctrl', function($scope

<% views.forEach(function(view, idx){ -%><% if (idx > 0) { %>,\n<% } -%>

    ) {


  });
`;

        var tplCode = (state.multipleViews) ? codeStateViews : codeState.join("");

        var ejs = require('ejs'); 
        var code = ejs.render(tplCode, state);

        console.log(code);

        return gulp.src(path.join(config.devDir,'app/scripts/app.js'), {
                base: config.devDir
            })
            .pipe(change(chgState))
            .pipe(gulp.dest(config.devDir));  
    }

    var questions = [{
        type: "input",
        name: "nome",
        message: "Qual o nome do novo 'controller'?"
    }, {
        type: 'checkbox',
        name: 'services',
        message: "Quais serviços devem ser injetados no 'controller'?",
        choices: ['$compile', '$document', '$http', '$locale', '$location', '$rootScope', '$state', '$window']
    }];


    // console.log('config: ', config);

    setTimeout(function() {
        inquirer.prompt(questions, function(answers) {
            console.log('answers: ', answers);
        });
    }, 1000);

});
