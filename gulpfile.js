var gulp = require('gulp');
var change = require('gulp-change');
var fs = require('fs');

require('require-dir')('./gulp-tasks', {recurse: true});

var chgData = {};


function chgState(content) {
    return content.replace(/(.state(?![\s\S]*.state)[\s\S]*?}\);)/g, chgData.state + '/* teste */\n\t\t\t$1');
}


gulp.task('testx', function() {
	var cfg = require('./gulp-tasks/config');

	chgData.state = "/** VAR TESTE **/\n\t\t\t";

    return gulp.src('.tmp/ani-theme/app/scripts/app.js', {base: '.tmp/'})
        .pipe(change(chgState))
        .pipe(gulp.dest(cfg.tmpDir));

	// console.log(process.cwd().split('\\').slice(-1).pop());
	// console.log(cfg.dataDir);

});


gulp.task('build', ['download'], function() {

});

gulp.task('default', ['test'], function() {
    //gulp.start('build');
});
