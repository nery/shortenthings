/* jshint node:true */
'use strict';

var gulp = require('gulp');
// var karma = require('karma').server;
var argv = require('yargs').argv;
var $ = require('gulp-load-plugins')();
var path = require('path');
var config = require('../config');

// console.log('xxx: ', require('main-bower-files', {bowerDirectory: 'dev/' , paths:{bowerJson: 'dev/bower.json'}}));

// gulp.task('html', ['styles'], function() {
//   var lazypipe = require('lazypipe');
//   var cssChannel = lazypipe()
//     .pipe($.csso)
//     .pipe($.replace, 'bower_components/bootstrap/fonts', 'fonts');

//   var assets = $.useref.assets({searchPath: '{.tmp,app}'});

//   return gulp.src('app/**/*.html')
//     .pipe(assets)
//     .pipe($.if('*.js', $.ngAnnotate()))
//     .pipe($.if('*.js', $.uglify()))
//     .pipe($.if('*.css', cssChannel()))
//     .pipe(assets.restore())
//     .pipe($.useref())
//     .pipe($.if('*.html', $.minifyHtml({conditionals: true, loose: true})))
//     .pipe(gulp.dest('dist'));
// });


gulp.task('fonts', function() {
  return gulp.src(
    require('main-bower-files')({paths: './dev'})
          .concat(path.join(config.devDir, 'bower_components/bootstrap/fonts/*'))
    )
    .pipe($.filter('**/*.{eot,svg,ttf,woff,woff2}'))
    .pipe($.flatten())
    .pipe(gulp.dest(path.join(config.devDir, '/app/fonts')));
});


gulp.task('extras', function() {
  return gulp.src([
    path.join(config.devDir, 'node_modules/apache-server-configs/dist/.htaccess')
  ], {
    dot: true
  })
  .pipe(gulp.dest(config.devDir));
});


gulp.task('watch', ['connect'], function() {
  $.livereload.listen();

  // watch for changes
  gulp.watch([
    path.join(config.devDir, 'app/**/*.html'),
    path.join(config.devDir, 'app/styles/**/*.css'),
    path.join(config.devDir, 'app/scripts/**/*.js'),
    path.join(config.devDir, 'app/images/**/*')
  ]).on('change', $.livereload.changed);

  // gulp.watch([
  //   'app/styles/**/*.css'
  // ], ['styles']);
  // gulp.watch('bower.json', ['inject']);
});


gulp.task('connect', function() {
  var serveStatic = require('serve-static');
  var serveIndex = require('serve-index');
  var app = require('connect')()
    .use(require('connect-livereload')({port: 35729}))
    .use(serveStatic(path.join(config.devDir, 'app')))
    /// .use(serveStatic('app'))
    // paths to bower_components should be relative to the current file
    // e.g. in app/index.html you should use ../bower_components
    .use('/bower_components', serveStatic(path.join(config.devDir, 'bower_components')))
    /// .use(serveIndex('app'));
    .use(serveIndex(path.join(config.devDir, 'app')));

  require('http').createServer(app)
    .listen(9000)
    .on('listening', function() {
      console.log('Started connect web server on http://localhost:9000');
    });
});


// gulp.task('serve', ['inject', 'connect', 'fonts', 'styles', 'watch'], function() {
// gulp.task('serve', ['html', 'styles', 'images', 'fonts', 'extras', 'connect', 'watch'], function() {
gulp.task('serve', ['fonts', 'extras', 'connect', 'watch'], function() {
  if (argv.open) {
    require('opn')('http://localhost:9000');
  }
});


// inject bower components
// gulp.task('inject', function() {
//     var inject = require('gulp-inject');
//     var paths = {
//         css: [
//             'bower_components/**/*min*.css',
//             'bower_components/**/*csp*.css',
//             'app/styles/**/*.css',
//             '!./www/lib/**'
//         ],
//         js: [
//             './bower_components/**/*-mocks.js',
//             './bower_components/**/*.min.js',
//             './app/scripts/**/*.js',
//             '!./bower_components/**/*es5*',
//             '!./bower_components/**/*json3*',
//             '!./bower_components/**/*sizzle*'
//         ]
//     };

//     return gulp.src('app/index.html')
//         .pipe(inject(
//             gulp.src(paths.css, {
//                 read: false
//             }), {
//                 relative: true
//             }))
//         .pipe(gulp.dest('.tmp'))

//     .pipe(inject(
//             gulp.src(paths.js, {
//                 read: false
//             }), {
//                 relative: true,
//                 ignorePath: '../app'
//             }))
//         .pipe(gulp.dest('dist'))
//         .pipe(gulp.dest('.tmp'));
// });



// gulp.task('builddist', ['jshint', 'jscs', 'html', 'images', 'fonts', 'styles', 'extras'],
//   function() {
//   return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
// });

// gulp.task('build', ['clean'], function() {
//   gulp.start('builddist');
// });


// gulp.task('clean', require('del').bind(null, ['.tmp', 'dist']));


// gulp.task('internal_buld', ['html', 'styles', 'images', 'fonts', 'extras'], function(){
// });


// gulp.task('build', ['clean'], function() {
//     gulp.start('internal_buld');
// });


gulp.task('docs', [], function() {
  return gulp.src('app/scripts/**/**')
    .pipe($.ngdocs.process())
    .pipe(gulp.dest('./docs'));
});


gulp.task('testex', function() {
    var wiredep = require('wiredep').stream;

    return gulp.src('app/index.html')
        .pipe(wiredep({
            bowerJson: require('./bower.json')
        }))
        .pipe(gulp.dest('app/'));
});



gulp.task('test', function(done) {
  karma.start({
    configFile: __dirname + '/test/karma.conf.js',
    singleRun: true
  }, done);
});


// List all files in a directory in Node.js recursively in a synchronous fashion
var walkSync = function(dir, filelist) {
    var fs = fs || require('fs'),
        files = fs.readdirSync(dir);

    filelist = filelist || [];
    files.forEach(function(file) {
        if (fs.statSync(dir + '/' + file).isDirectory()) {
            filelist = walkSync(dir + '/' + file, filelist);
        } else {
            filelist.push(dir + '/' + file);
        }
    });
    return filelist;
};


gulp.task('teste1', function() {
    var lib = require('bower-files')();
  var concat = require('gulp-concat');
  var uglify = require('gulp-uglify'); 

  var flist = walkSync('./bower_components');
    console.log(flist);

    gulp.src(lib.ext('css').files)
        .pipe(concat('lib.min.css'))
        /*.pipe(uglify())*/
        .pipe(gulp.dest('lib'));  

    return gulp.src(lib.ext('js').files)
        .pipe(concat('lib.min.js'))
       /* .pipe(uglify())*/
        .pipe(gulp.dest('lib'));
});

